<?php

namespace Kubomikita;

use Nette\DI\Container;

interface IRouter {
	public static function create(Container $container);
}
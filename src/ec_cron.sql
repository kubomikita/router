-- Adminer 4.6.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `ec_cron`;
CREATE TABLE `ec_cron` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL COMMENT 'PHP trieda obsluhujúca plánovanú úlohu.',
  `args` text COMMENT 'JSON vstup sa vkladá ako prvý parameter $args typu array do triedy definovanej v stĺpci model.',
  `minute` varchar(20) NOT NULL,
  `hour` varchar(20) NOT NULL,
  `day` varchar(20) NOT NULL,
  `month` varchar(20) NOT NULL,
  `weekday` varchar(20) NOT NULL,
  `priority` int(11) NOT NULL,
  `enabled` int(11) NOT NULL DEFAULT '0',
  `last` datetime NOT NULL,
  `log` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Automaticky spúšťané skripty';

INSERT INTO `ec_cron` (`id`, `name`, `model`, `args`, `minute`, `hour`, `day`, `month`, `weekday`, `priority`, `enabled`, `last`, `log`) VALUES
(1,	'Export XML: heureka.sk',	'ExportXmlJob',	'{\"format\":\"Heureka\"}',	'*/10',	'*',	'*',	'*',	'*',	6,	0,	'0000-00-00 00:00:00',	''),
(2,	'Export XML: heureka.sk (dostupnostný feed)',	'ExportXmlJob',	'{\"format\":\"HeurekaStocks\"}',	'*',	'*',	'*',	'*',	'*',	7,	1,	'0000-00-00 00:00:00',	''),
(3,	'Import Faktúr z Pohody',	'PohodaImportInvoices',	'',	'*',	'*',	'*',	'*',	'*',	7,	0,	'0000-00-00 00:00:00',	''),
(4,	'Odoslanie SMS o výpadku Pohody',	'PohodaSyncOff',	'',	'*/20',	'*',	'*',	'*',	'*',	7,	0,	'0000-00-00 00:00:00',	''),
(5,	'Balík zabalený & Watchdog',	'Watchdog',	'',	'*',	'*',	'*',	'*',	'*',	7,	0,	'0000-00-00 00:00:00',	''),
(6,	'Neaktivovaný zákaznik',	'PartnerNoActivated',	'',	'*',	'*',	'*',	'*',	'*',	7,	0,	'0000-00-00 00:00:00',	''),
(7,	'Export XML: prva.sk',	'ExportXmlJob',	'{\"format\":\"Prva\"}',	'*/30',	'*',	'*',	'*',	'*',	6,	1,	'0000-00-00 00:00:00',	''),
(8,	'Rehash hesiel pre admina',	'PasswordRehash',	'{\"table\":\"ec_users\"}',	'*',	'*',	'*',	'*',	'*',	6,	0,	'0000-00-00 00:00:00',	''),
(9,	'Rehash hesiel pre zakaznikov',	'PasswordRehash',	'{\"table\":\"ec_partneri_data\"}',	'*',	'*',	'*',	'*',	'*',	6,	0,	'0000-00-00 00:00:00',	'');

-- 2018-02-27 21:42:18

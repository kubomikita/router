<?php
namespace Kubomikita\Ajax;

use Kubomikita\Commerce\AppRouter;
use Kubomikita\Commerce\DatabaseService;
use Kubomikita\Commerce\IDiConfigurator;
use Kubomikita\Form\Response\Alert;
use Kubomikita\IRouter;
use Latte\Engine;
use Nette\Application\AbortException;
use Nette\Application\BadRequestException;
use Nette\Database\Connection;
use Nette\DI\Container;
use Nette\Http\Request;
use Nette\Http\Url;
use Nette\Security\User;
use Nette\Utils\Callback;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Tracy\Debugger;

class Router implements IRouter {
	/** @var \Nette\Http\Request */
	public $httpRequest;
	/** @var IDiConfigurator */
	public $context;

	public static function create( Container $container ) {
		$conf = $container->getByType(IDiConfigurator::class);
		$ajax = new self($conf);
		/** @var Connection $db */
		$db = $container->getByType(Connection::class);
		DatabaseService::disconnect($db);
		exit;
	}

	/**
	 * Router constructor.
	 *
	 * @param $configurator \Kubomikita\Commerce\IDiConfigurator
	 */
	public function __construct(IDiConfigurator $configurator) {

		\LangStr::$locale = $_SESSION["lang"];

		$this->context = $configurator;
		$this->httpRequest = $configurator->getService("request");

		$response = null;

		try {
			ob_start();
			if ( $this->httpRequest->getQuery( "mod" ) ) {
				$this->old();
				/** @var Connection $db */
				//$db = $this->context->getByType( Connection::class );
				//DatabaseService::disconnect( $db );
				//exit;
			} elseif ( strpos( $this->httpRequest->getUrl()->getPath(), "mod." ) !== false ) {
				/** @var User $User */
				$User = $configurator->getService( "user" );
				$User->getStorage()->setNamespace( "admin" );
				init_auth_ajax( $User );
				$qry = $this->httpRequest->getUrl()->getQueryParameters();
				$qry += $this->httpRequest->getPost();
				//dump($qry,$this);
				//dump(class_exists("Mod_ObjednavkaDodavatel"),get_declared_classes());
				//exit;
				call_user_func( "action_" . $qry["action"], $qry );
				/** @var Connection $db */
				$db = $this->context->getByType( Connection::class );
				DatabaseService::disconnect( $db );
				exit;
			} elseif ( strpos( $this->httpRequest->getUrl()->getPath(), "rpc.php" ) !== false ) {
				$this->oldAdminAjaxHandler();
				/** @var Connection $db */
				$db = $this->context->getByType( Connection::class );
				DatabaseService::disconnect( $db );
				exit;
			} elseif ( strpos( $this->httpRequest->getUrl()->getPath(), "ajaxHandler.php" ) !== false ) {
				$this->oldAjaxHandler();

				//exit;
			} elseif ( $this->httpRequest->getQuery( "render" ) ) {
				$model  = $this->httpRequest->getQuery( "controller" );
				$action = $this->httpRequest->getQuery( "action" );
				$params = $this->httpRequest->getQuery( "params" );
				//$n = new $model;
				$n = $configurator->getService( "controller" )->createOne( $model );
				echo $n->{$action}( $this, $params );
			} else {
				$model  = $this->httpRequest->getQuery( "controller" );
				$action = $this->httpRequest->getQuery( "action" );

				if ( $model == null or $action == null ) {
					//echo 'Bad request';
					throw new AbortException( "Bad ajax request" );
					exit;
				}

				$controller = $this->context->getService( "controller" )->createOne( $model );


				if ( ! method_exists( $controller, $action ) ) {
					throw new AbortException( "Bad ajax request. Controller or Action not defined. (not exists)" );
					exit;
				}
				/** @var User $User */
				//$User = $configurator->getService("user");
				//$User->getStorage()->setNamespace("admin");
				//init_auth($User);

				$approuter = new AppRouter();
				$seolink   = $this->httpRequest->getQuery( "seolink" );
				if ( strlen( $seolink ) > 0 ) {
					$approuter = AppRouter::findByLink( $seolink );
				}
				if ( Callback::isStatic( [ $controller, $action ] ) ) {
					$static = $model . "::" . $action;
					if ( version_compare( PHP_VERSION, "7.0.0" ) >= 0 ) {
						echo $static( $this, $approuter );
					} else {
						echo call_user_func( $static, $this, $approuter );
					}
				} else {
					//echo "calling non static method";
					echo  [ $controller, $action ]( $this, $approuter );
				}
			}

			$response = ob_get_clean();
		} catch (BadRequestException $e){
			$r = new Alert();
			$r->setDanger();
			$r->setReload(true, 3000);
			$r->title = $e->getCode() . " ".end(explode("\\",get_class($e)));
			$r->message = $e->getMessage();
			echo $r->render();
		} catch (\Exception $e){
			echo '<div class="alert alert-danger mb-0"> <i class="fas fa-exclamation-triangle mr-2"></i> '.$e->getMessage().'</div>';
			exit;
		}

		if($response !== null){
			try {
				$json = Json::decode($response);
				echo $response;
			} catch (JsonException $e){
				echo $response;
				/** @var \Kubomikita\Commerce\DataLayer\Container $dataLayerContainer */
				$dataLayerContainer = $this->context->getByType(\Kubomikita\Commerce\DataLayer\Container::class);
				bdump($dataLayerContainer->getDataLayer(),"AJAX DataLayer");
				echo $dataLayerContainer->setType(\Kubomikita\Commerce\DataLayer\Container::CONTAINER_AJAX)->getContainer();
			}
		}

		/** @var Connection $db */
		//$db = $this->context->getByType( Connection::class );
		//DatabaseService::disconnect( $db );

	}

	public function getQuery($key = null){
		if($key === null) {
			return $this->httpRequest->getQuery();
		}
		return $this->httpRequest->getQuery($key);
	}

	public function oldAdminAjaxHandler(){
		/*if(defined("APP_DIR")) {
			include APP_DIR . "manager/lib.menu.php";
		} else {
			include __DIR__ . "/../../../../manager/lib.menu.php";
		}*/
		\AJAX::register('menu_tab','\Menu','setActiveTab');
		\AJAX::register('lang','\Menu','setLang');
		\AJAX::call($_REQUEST['m']);
	}
	public function oldAjaxHandler(){
		$condition = true;
		
		if($condition){
			if($_GET["function"]){
				if($_GET["type"]=="content"){
					$func=$_GET["function"];
					if(function_exists($func)){
						call_user_func($func,$_GET);
					} else {
						echo '<div class="alert alert-danger"><b>AjaxContent:</b> Funkcia '.$func.' neexistuje.</div>';
					}

				} else {
					$func=$_GET["function"];
					$method="_".strtoupper($_GET["method"]);
					$m=$_GET["method"];
					if($m == "POST"){
						$data = $_POST;
					} elseif($m == "GET"){
						$data = $_GET;
					}

					$ajax=$data["ajax"];
					$spam=$data["my_email"];

					if(strtolower($_SERVER["REQUEST_METHOD"]) == strtolower($m)){

						if($ajax and $spam == "nospam"){
							unset($_GET["function"]);
							unset($_GET["method"]);
							unset($data["ajax"]);
							unset($data["my_email"]);
							$req=array();
							$valid=true;
							if($_GET["ajax_load"]){
								$value = $data["value"];
								$ses=$_SESSION["FormRequired"][$data["formId"]][$data["input_name"]];
								if(!empty($ses)){
									if(trim($value) == ""){
										$req[]=$ses;
										$req_js[] = "#".$data["formId"]." input[name=\'".$data["input_name"]."\']";
										$valid=false;
									}

								}
								$ses=$_SESSION["FormPattern"][$data["formId"]][$data["input_name"]];
								if(!empty($ses)){
									$ch = \FormItem::checkPattern($ses["pattern"],$value);
									if($ch !== null){
										if($ch == false){
											if(trim($value) != ""){
												$req[]=$ses["patternText"];
												$req_js[] = "#".$data["formId"]." input[name=\'".$data["input_name"]."\']";
											}
											$valid=false;
										}
									}
								}
							} else {
								$EQUALTO = $_SESSION["FormEqualTo"][$data["formId"]];
								if(!empty($EQUALTO)){
									foreach ($EQUALTO as $key => $v) {
										$ddd = $_SESSION["FormDependsOn"][$data["formId"]][$key];
										$dddd = (bool)$data[$ddd];
										if($ddd != null and !$dddd) { continue;}
										if($data[$key] !== $data[$v["field"]]){
											$req[]=$v["text"];
											$req_js[] = "#".$data["formId"]." input[name=\'$key\']";
											$valid=false;
										}
									}
								}
								if(!empty($_SESSION["FormRequired"][$data["formId"]])){
									foreach($_SESSION["FormRequired"][$data["formId"]] as $name => $val){
										$ddd = $_SESSION["FormDependsOn"][$data["formId"]][$name];
										$dddd = (bool)$data[$ddd];
										if($ddd != null and !$dddd) { continue;}
										if(trim($data[$name]) == ""){
											$req[]=$val;
											$req_js[] = "#".$data["formId"]." input[name=\'$name\'], #".$data["formId"]." textarea[name=\'$name\'], #".$data["formId"]." select[name=\'$name\']";
											$valid = false;
										}
									}

								}
								if(!empty($_SESSION["FormPattern"][$data["formId"]])){
									foreach($_SESSION["FormPattern"][$data["formId"]] as $name => $val){
										$ddd = $_SESSION["FormDependsOn"][$data["formId"]][$name];
										$dddd = (bool)$data[$ddd];
										if($ddd != null and !$dddd) { continue;}
										$ch = \FormItem::checkPattern($val["pattern"],trim($data[$name]));
										if($ch !== null){
											if($ch == false){
												if(trim($data[$name]) != ""){
													$req[]=$val["patternText"];
													$req_js[] = "#".$data["formId"]." input[name=\'$name\'],#".$data["formId"]." select[name=\'$name\'],#".$data["formId"]." textarea[name=\'$name\']";
												}
												$valid=false;
											}
										}
									}
								}
							}

							if(!$valid){
								echo '<div class="alert alert-danger">';
								foreach ($req as $r) {
									echo "<i class=\"fa fa-times\"></i> ".$r . "<br>";
								}
								echo '</div>';
								echo '<script>';
								foreach($req_js as $rr){
									echo "setInputClass('$rr','has-error');";
								}
								echo '</script>';
							}

							if(my_function_exists($func)){
								$dt = $data;
								if(!empty($_FILES)){
									$dt = array();
									$dt = array_merge($data,$_FILES);
								}

								call_user_func($func,$dt,$ajax,$m,$valid);
							}
						}
					} else {
						echo "<b>Error:</b> metoda odoslania nesuhlasi";
					}
				}
			}
		} else {
			header('Content-type: text/html; charset=UTF-8');
			//echo '<div style="color:red;margin-bottom:20px;">Pravdepodobne používate <strong>zastaralý prehliadač</strong>, to je príčinou nefunkčnosti našej stránky. <strong>Odporúčame Vám</strong> nainštalovať si <strong>novšiu verziu prehliadača</strong>, alebo skúste si stiahnuť a nainštalovať <a href="http://www.google.com/chrome/">Google Chrome</a> prehliadač. Môžete taktiež <strong>využiť, telefonickú objednávku (+421 914 777 222) alebo objednávku <a href="mailto:shop@protein.sk">e-mailom</a></strong>.</div>';
			//echo "<div style=\"color:red\">Ak sa Vam tato chyba zobrazila po odoslani formulara, je mozne ze Vas prehliadac ma nacitanu staru verziu stranky. Prosim obnovte stranku pomocou kombinacie tlacitiel <b>CTRL + F5</b>. Ak sa Vam tato chyba objavy znova, skuste to este raz alebo nas kontaktuje na <b>+421 914 777 222</b> alebo e-mailom <a href=\"mailto:shop@protein.sk\"><b>shop@protein.sk</b></a></div>";
		}
	}
	/**
	 * Back compatibility - Render ajax request
	 */
	public function old() {

		if($this->httpRequest->getQuery("mod") == "kosik_hover"){
			echo "@deprecated";
		}
		if($this->httpRequest->getQuery("mod") == "subcategories"){
			//include "commerce/view/CategoryList/Subcategory.phtml";
			echo "@deprecated";
		}
		if($this->httpRequest->getQuery("mod") == "miniCart"){
			\Cart_controller::handleMiniCart();
		}
		if($this->httpRequest->getQuery("mod")=='kosik'){
			\Cart_controller::handleHeaderCart($this);
		};
		if($this->httpRequest->getQuery("mod")=='kosik_order'){
			\Cart_controller::handleCart($this);
		};
		if($this->httpRequest->getQuery("mod")=='kosik_order_step1'){
			\Cart_controller::handleOrderStep1();
		};
		if($this->httpRequest->getQuery("mod")=='kosik_added' && $this->httpRequest->getQuery("tovar")){
			\Cart_controller::handleAdded($this->httpRequest);
		};
		if($this->httpRequest->getQuery("mod")=="vyrobca_load"){

			$Vyrobcovia_new = \TovarVyrobca::fetch($this->httpRequest->getQuery("type"));
			$expect = strtoupper(substr($this->httpRequest->getQuery("type"),0,1));
			echo '<ul id="vyrobcovia" >';
			foreach($Vyrobcovia_new as $V){
				//dump($V->image());
				if($V->count_items() > 0){
					echo '<li><a href="'.eshop_vyrobca_seo_link_page($V,1).'">'.$V->nazov.' '.$V->tag_show($expect).'</a></li>';
				}
			}
			echo '</ul>';
		}

		if($this->httpRequest->getQuery("mod")=="control" and $this->httpRequest->getQuery("controller")){
			$data = (array) json_decode($this->httpRequest->getQuery("data"));
			//dump($this,$data,$this->httpRequest->getQuery("controller"));
			return call_user_func($this->httpRequest->getQuery("controller"),$data);
		}
		if($this->httpRequest->getQuery("mod") == "sameAs" and $this->httpRequest->getQuery("tovar")){
			$T=new \Tovar($this->httpRequest->getQuery("tovar"));
			if($T->id >0){
				$result = $T->fetch_social(3);
				if(count($result) > 0 ){
					echo '<div class="row odporucame">';
					echo '<div class="col-xs-12">';
					echo '<div class="title">'.__("K tomuto produktu si zákazníci zakúpili tiež").':</div>';
					foreach ($result as $Tovar){
						echo '<a class="odp-item" href="'.eshop_seo_link($Tovar).'">';
						echo '<img src="'.$Tovar->image()->resizedCachedUrl("fit",81,81,"ffffff").'">';
						echo '<div class="odp-product">';
						echo '<div class="manufacturer">'.$Tovar->vyrobca->nazov.'</div>';
						echo '<div class="name">'.$Tovar->nazov.'</div>';
						echo '<div class="price">od '.\Format::money_user($Tovar->predajna_cena_user()->suma()).'</div>';
						echo '</div>';
						echo '</a>';
					}

					echo '</div>';
					echo '</div>';
				}
			}
		}

		if($this->httpRequest->getQuery("mod")=='tovarhodnotenie'){
			$Tovar = new \Tovar($this->httpRequest->getQuery("tovar"));
			$TH = new \TovarHodnotenie($Tovar->id);
			$TH->tovar_object = $Tovar;
			$TH_CURRPAGE = (int)$this->httpRequest->getQuery("page");
			$TH_LIST = $TH->fetchApproved($TH_CURRPAGE);
			$TH_PAGES = ceil($TH->pocet()/$TH->pagesize);
			include('commerce/view/ProductDetail/Rating.phtml');
		};

		if($this->httpRequest->getQuery("mod")=='tovarotazka'){
			$Tovar = new \Tovar($this->httpRequest->getQuery("tovar"));
			$TO_COUNT = \TovarOtazka::pocet($Tovar->id);
			$TO_CURRPAGE = (int)$this->httpRequest->getQuery("page");
			$TO_LIST = \TovarOtazka::fetchTovar($Tovar,$TO_CURRPAGE);
			$TO_PAGES = ceil($TO_COUNT/\TovarOtazka::$pagesize);
			include('commerce/view/ProductDetail/Answer.phtml');
		};
		/**
		USER functions

		 */
		if($this->httpRequest->getQuery("mod")=="predajna_zasoba" && $this->httpRequest->getQuery("tovar_id")){
			\Predajne_controller::handleTovarPredajne($this);
		}
		if($this->httpRequest->getQuery("mod")=='favorites_added'){
			include('commerce/view/ProductDetail/AddedToFavorites.php');
		};
		if($this->httpRequest->getQuery("mod")=='user_fa_add'){
			$controller = new \LoginForm_controller();
			$controller->setLatteTemplate("UserInvoiceAddress.latte");
			$rendered = $controller->objRender(null, []);
			echo $rendered;
		};
		if($this->httpRequest->getQuery("mod")=='user_da_add'){
			$controller = new \LoginForm_controller();
			$controller->setLatteTemplate("UserDeliveryAddress.latte");
			$rendered = $controller->objRender(null, []);
			echo $rendered;
		};
		if($this->httpRequest->getQuery("mod")=='user_ad_delete'){
			$controller = new \LoginForm_controller();
			$controller->setLatteTemplate("UserDeleteAddress.latte");
			echo $controller->objRender(null,[]);
		};
		if($this->httpRequest->getQuery("mod")=='my_account'){
			include('commerce/view/LoginForm/MyAccount.phtml');
		};
		if($this->httpRequest->getQuery("mod")=='user_details'){
			include('commerce/view/LoginForm/UserDetails.phtml');
		};
		if($this->httpRequest->getQuery("mod")=='detail_objednavky'){
			include('commerce/view/Cart/OrderDetail.phtml');
		};
		if($this->httpRequest->getQuery("mod")=='delete_dialog'){
			include('commerce/view/HomePage/DeleteDialog.phtml');
		};
		if($this->httpRequest->getQuery("mod")=='watchdog'){
			$watch = $this->httpRequest->getQuery("watch");//$_GET["watch"];
			$tovar = $this->httpRequest->getQuery("tovar");//$_GET["tovar"];
			//include('commerce/view/ProductDetail/Watchdog.phtml');
			\ProductDetail_controller::handleWatchdog($this);
		};
		if($this->httpRequest->getQuery("mod")=='addRating'){
			include('commerce/view/ProductDetail/AddRating.phtml');
		};
		if($this->httpRequest->getQuery("mod")=='addQuestion'){
			include('commerce/view/ProductDetail/AddQuestion.phtml');
		};
		if($this->httpRequest->getQuery("mod")=='instantsearch'){

			\SearchForm_controller::actionInstantSearch($this->httpRequest);
			/*ini_set("display_errors",true);
			error_reporting(E_ALL);
			$Search = new \TovarSearchResult();
			$Search->indexable=0;
			$Search->order_by='predajnost desc';
			$Search->fulltext=strip_tags(addslashes($this->httpRequest->getQuery("q")));
			$Result = $Search->result_part(0,5);
			mysqli_query(\CommerceDB::$DB,"INSERT INTO ec_search_words (`keywords`,`browser`,`user`,`ip`,`result`,`datum`) VALUES('$Search->fulltext','$_SERVER[HTTP_USER_AGENT]','$_SESSION[auth_web_uid]','$_SERVER[HTTP_X_REAL_IP]','".count($Result)."',NOW())");
			if(sizeof($Result)){
				foreach($Result as $T){
					echo('<a class="instant-search-item clearfix" href="'.eshop_seo_link($T).'">');
					echo('<div class="instant-search-item-text"><div class="nazov">'.$T->nazov.'</div>cena od: <span class="cena">'.$T->predajna_cena_user()->format_suma().'</span></div>');
					echo('<img class="instant-search-item-img" src="'.$T->image()->resizedCachedUrlName('fit','40','40','#ffffff').'" alt="'.$T->nazov.'">');
					echo('</a>');
				};
				echo '<a href="javascript:;" onClick="$(\'.topSearch\').submit();" class="instant-search-showall btn btn-lg btn-success">'.__("Zobraziť všetky výsledky").'...</a>';
			} else {
				echo('<div class="isrch_msg">'.__("nenašlo sa").' ...</div>');
			};*/

		}

	}
}
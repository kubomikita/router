<?php

namespace Kubomikita\Api;

use Kubomikita\Cli\Cron\BaseJob;
use Kubomikita\Commerce\DatabaseService;
use Kubomikita\Commerce\IDiConfigurator;
use Kubomikita\IRouter;
use Nette\Application\AbortException;
use Nette\Application\ApplicationException;
use Nette\Application\BadRequestException;
use Nette\Application\ForbiddenRequestException;
use Nette\Application\Routers\Route;
use Nette\Database\Connection;
use Nette\DI\Container;
use Nette\Http\IRequest;
use Nette\Http\Request;
use Nette\Http\RequestFactory;
use Nette\InvalidArgumentException;
use Nette\Utils\Reflection;
use Nette\Utils\Strings,
	Nette\Utils\Finder;

class Router implements IRouter {
	/** @var IDiConfigurator  */
	private $context;
	/** @var IRequest */
	private $request;
	/** @var IRequest */
	private $globalRequest;
	/** @var string  */
	private $class;
	/** @var string  */
	private $method;
	private $args;
	private $files;
	
	public static function create( Container $container ) {
		$conf = $container->getByType(IDiConfigurator::class);
		$request = $container->getByType(Request::class);
		$globalRequest = (new RequestFactory())->createHttpRequest();
		$api = new self( $request, $globalRequest, $conf);
		/** @var Connection $db */
		$db = $container->getByType(Connection::class);
		DatabaseService::disconnect($db);
		exit;
	}

	public function __construct(IRequest $request, IRequest $globalRequest, IDiConfigurator $context) {
		$this->request = $request;
		$this->globalRequest = $globalRequest;
		$this->context = $context;

		$query = $this->request->getQuery( "query" );
		if((String) $query == ""){
			throw new BadRequestException("Not found.",404);
		}


		$class= $method = $args = null;
		$e = explode("/",$query);
		$class = $e[0];
		if(isset($e[1])){
			$method = $e[1];
		}
		if(isset($e[2])){
			$args = $e[2];
		}
		$args = ($args != null) ? explode(";",$args): [];
		foreach($args as $key=> $a){
			if(strpos($a,":") !== false) {
				list( $k, $v ) = explode( ":", $a );
				if ( $v !== null ) {
					$args[ $k ] = $v;
					unset( $args[ $key ] );
				}
			}
		}
		$args += $_REQUEST;
		$this->files = $_FILES;
		unset($args["api"]);
		unset($args["query"]);

		$this->class = "\\Kubomikita\\Api\\".Route::path2presenter($class);

		$this->method = "action";
		$em = explode("_",$method);
		bdump($method);
		foreach($em as $mm){
			$this->method .= Strings::firstUpper($mm);
		}
		$this->method = Route::path2action($this->method);

		foreach($args as $ak=> $ar){
			if(isset($_COOKIE[$ak])){
				unset($args[$ak]);
			}
		}
		//dumpe($this);
		//dumpe($this->method,Route::action2path($this->method),Route::path2action($this->method));
		//bdump($this);
		$this->args = $args;
		try {
			if($this->method == "action"){
				throw new BadRequestException("Action is not specified");
			}
			if (!class_exists($this->class)) {
				throw new BadRequestException("Class '" . $this->class . "' not found.");
			}
			/** @var BaseApi $job */
			$job = new $this->class($this->context,$this, $this->request);
			if(!($job instanceof IApi)){
				throw new ApplicationException("Class '".$this->class."' must implements IApi");
			}

			if(!method_exists($this->class,$this->method)){
				throw new BadRequestException("Method '".$this->method."' in class '".$this->class."' not found");
			}

			$httpMethod = $job->reflection->getMethod($this->method)->getAnnotation("method");
			if($httpMethod !== null){
				if ( Strings::lower( $this->globalRequest->getMethod() ) !== Strings::lower( $httpMethod ) ) {
					throw new ApplicationException( "Only '$httpMethod' request method is allowed, but '".$this->globalRequest->getMethod()."' processed.", 500 );
				}
			}

			$job->setArgs($this->args);
			$job->beforeStartup();
			$job->authenticate();

			$routerArgs = $job->getArgs();
			$actionArgs = [];
			$actionParameters = $job->reflection->getMethod($this->method)->getParameters();
			
			foreach($actionParameters as $param){
				if(isset($routerArgs[$param->name])) {
					if(empty($routerArgs[$param->name]) && $routerArgs[$param->name] !== "0" && !$param->isOptional()){
						throw new BadRequestException("Missing value of argument '\${$param->name}' in method '{$this->class}::{$this->method}()'");
					}
					$paramType = Reflection::getParameterType($param);
					$actionArgs[$param->name] = $routerArgs[$param->name];
					if($paramType !== null){
						settype($actionArgs[$param->name], $paramType);
					}
				} else {
					if(!$param->isOptional()) {
						throw new BadRequestException("Missing argument '\${$param->name}' in method '{$this->class}::{$this->method}()'");
					}
				}
			}
			$job->logRequest($this->globalRequest);
			$job->{$this->method}(... array_values($actionArgs));

		} catch(\Throwable $e){
			trigger_error($e->getMessage(),E_USER_NOTICE);
			if($this->context->isDebugMode()){
				bdump($e);
			}
			if(isset($job) && $job instanceof IApi){
				$job->logRequest($this->globalRequest, $e);
			}

			throw $e;
		}
	}

}
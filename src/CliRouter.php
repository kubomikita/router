<?php
namespace Kubomikita\Cli;

use Cron\CronExpression,
	Nette\Utils\Strings,
	Nette\Utils\Json,
	Nette\Utils\Finder,
	Tracy\Debugger;
use Kubomikita\Commerce\DatabaseService;
use Kubomikita\Commerce\IDiConfigurator;
use Kubomikita\IRouter;
use Kubomikita\Service;
use Nette\Database\Connection;
use Nette\DI\Container;
use Nette\Utils\DateTime;

class BaseRouter implements IRouter {
	protected static $tableName = "ec_cron";
	public $db;
	/** @var Container */
	public $context;
	protected $jobName = null;
	protected $jobArgs = [];

	public static function create( Container $container ) {
		$cli = new self( $_SERVER['argv'], $container);
		/** @var Connection $db */
		$db = $container->getByType(Connection::class);
		DatabaseService::disconnect($db);
		exit;
	}

	/**
	 * @param string
	 * @return void
	 */
	protected function log($message)
	{
		$line = date(DATE_RFC2822) . "\t" . get_class($this) . "\t" . $message ."\n";
		echo $line;
	}

	public function __construct($args = null, Container $container) {
		$this->db = $container->getByType(Connection::class);
		$this->context = $container;
		//var_export(session_status());
		//exit;
		if($args == null){
			$args = $_SERVER["argv"];
		}
		if(!isset($args[1])){
			$args[] = "CronJobs";
		}

		$this->startup();
		$this->jobName = $args[1];
		if(isset($args[2]) && is_string($args[2])){
			$this->jobArgs = (array) json_decode($args[2]);
		} else {
			$this->jobArgs = $args[2];
		}
		$this->functionName = "action".ucfirst($this->jobName);
		try{
			if(!method_exists($this,$this->functionName)){
				throw new \Exception("Command line action not exists.");
			}
			$this->{$this->functionName}($this->jobArgs);
		} catch(\Exception $e){
			$this->log($e->getMessage()." - ".$e->getFile()." - ".$e->getLine()." - ".$e->getTraceAsString());
			//var_dump($e);
		}
	}
	private function checkManualStart($cronid,$args){
		if(isset($args["manual"])){
			$c = CronExpression::factory($args["manual"]);
			$fields = [
				"minute"=>$c->getExpression(0),
				"hour" => $c->getExpression(1),
				"day" => $c->getExpression(2),
				"month" => $c->getExpression(3),
				"weekday" => $c->getExpression(4),
			];
			unset($args["manual"]);
			$fields["args"] = Json::encode($args);
			$this->db->query("UPDATE ".self::$tableName." SET", $fields ,"WHERE id = ?",$cronid);
		}

	}
	private function setLast($cronid,$log=""){
		$this->db->query("UPDATE ".self::$tableName." SET",["last"=>new DateTime(),"log"=>$log],"WHERE id = ?",$cronid);
	}
	private function actionCronJobs(){
		//try {
			$q = $this->db->query( "SELECT * FROM " . self::$tableName . " WHERE enabled=1 ORDER BY priority" );
			foreach ( $q as $cron ) {
				$expression = implode( ' ', [ $cron->minute, $cron->hour, $cron->day, $cron->month, $cron->weekday ] );
				if ( CronExpression::isValidExpression( $expression ) ) {
					$time = CronExpression::factory( $expression );
					if ( $time->isDue() ) {
						try {
							$name  = $cron->model;
							$class = '\\Kubomikita\\Cli\\Cron\\' . $cron->model;
							if ( ! class_exists( $class ) ) {
								throw new \Exception( "Class '\\Kubomikita\\Cli\\Cron\\" . Strings::firstUpper( $name ) . "' not found." );
							}
							$args = $cron->args ? Json::decode( $cron->args, Json::FORCE_ARRAY ) : [];
							$job  = new $class( $this );
							if ( ! ( $job instanceof ICli ) ) {
								throw new \Exception( "Class '$class' not implements ICli" );
							}

							$job->run( $args );
							$this->checkManualStart( $cron->id, $args );
							$this->setLast( $cron->id, $job->getLog() );

						} catch ( \Exception $e ) {
							trigger_error( $e->getMessage(), E_USER_ERROR );
						}
					}

				} else {
					Debugger::log( "Cron ID '" . $cron->id . "' time expression could not be parsed.",
						Debugger::WARNING );
				}

			}
		/*} catch (\Exception $e){
			echo $e->getMessage();
		}*/

		exit;
	}

	private function actionTestCli(){
		print_r($this->db);
		print_r($this->context->parameters);
		echo "Hello world";
	}

	private function actionCmsImgToFileStore(){
		echo 'start'.PHP_EOL;
		$this->db = Service::get("database");
		$q = $this->db->query("SELECT * FROM cms_img_images WHERE img IS NOT NULL ORDER BY id DESC LIMIT 100");
		$path = WWW_DIR."bindata/";
		foreach($q as $r){
			$name = "cms-".$r->id.".jpg";
			echo 'Creating '.$name.PHP_EOL;
			file_put_contents($path.$name,$r->img);
			$this->db->query("UPDATE cms_img_images SET img=? WHERE id = ?",NULL,$r->id);
		}
		echo 'end'.PHP_EOL;
	}
	private function actionCommerceImgToFileStore(){
		echo 'start'.PHP_EOL;
		$this->db = Service::get("database");
		$q = $this->db->query("SELECT * FROM ec_img_images WHERE img IS NOT NULL ORDER BY id DESC LIMIT 100");
		$path = WWW_DIR."bindata/";
		$this->log($path);
		foreach($q as $r){
			$name = "ei-".$r->id.".jpg";
			echo 'Creating '.$name.PHP_EOL;
			file_put_contents($path.$name,$r->img);
			//var_export($r);
			$this->db->query("UPDATE ec_img_images SET img=? WHERE id = ?",NULL,$r->id);
		}
		echo 'end'.PHP_EOL;
	}
	private function actionCategorySvg(){
		$dir = WWW_DIR."assets/img/icons-menu/";
		/** @var \Kategoria $K */
		foreach(\Kategoria::fetchList() as $K){
			$icon = isset($K->metadata["icon"]) ? $K->metadata["icon"] : null;
			//$this->log($K->metadata["icon"]);
			if($icon === null || trim($K->metadata["icon"]) == ""){
				//$this->log($K->seo_name_new);
				if(file_exists($dir.$K->seo_name_new.".svg")) {
					$this->log( $K->nazov . " - " . $K->seo_name_new );
					$K->metadata["icon"] = $K->seo_name_new;
					$K->save();
				}
			}

		}
	}
	private function tovar_get_by_code($kod) {
		$q = $this->db->query("select * from ec_tovar where kod_systemu=?",$kod)->fetch();
		if(!$q){
			return new \Tovar;
		}
		return new \Tovar($q->id);
	}
	public function actionImgByCode(){
		$dir = TEMP_DIR."images_import";
		if(!file_exists($dir)){
			throw new \Exception("Images import dir '$dir' not exists");
		}

		$a = $f = $s = 0;

		foreach(glob($dir."/*.[jJp][pPn][gG]") as $file){
			$e = explode("/",$file);
			$filename = end($e);
			$end = explode(".",$filename);
			$ext = end($end);
			$code = trim(substr($filename,0,stripos($filename,".".$ext)));
			$t = $this->tovar_get_by_code($code);

			if((int) $t->id > 0) {
				$images = count($t->images());

				$this->log( $code . " - " . $t->nazov." - ". $images );
				if($images == 0) {
					$content = file_get_contents( $file );
					if ( trim( $content ) != "" ) {
						/** @var \TovarImg $img */
						foreach ( $t->images() as $img ) {
							$img->delete();
						}
						$I          = new \TovarImg();
						$I->tovarId = $t->id;
						$I->content = $content;
						$I->save();
						$I->saveData();
						//$I->moveTop()
						unlink( $file );
						//$this->log("ukladam novy obrazok");
						$a ++;
					}
					if ( $images < 1 and $t->primarna_kategoria->id != 1000 ) {
						$t->aktivny = 1;
						$t->sync    = 1;
						$t->save();
						$s ++;
					}
				} else {
					unlink( $file );
				}

			} else {
				$f++;
				$this->log("Tovar s kodom '$code' neexistuje.");
			}
		}
		$this->log("--------------------------------------------------");
		$this->log("Priradene: ".$a);
		$this->log("Aktivovane: ".$s);
		$this->log("Kod neexistuje: ".$f);
		$this->log("--------------------------------------------------");
	}
	private function startup(){

	}
}
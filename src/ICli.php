<?php

namespace Kubomikita\Cli;

interface ICli {

	/**
	 * @param array $args
	 *
	 * @return void
	 */
	public function run(array $args = []);
}
<?php
namespace Kubomikita\Commerce;

use Kubomikita\Service;
use Nette\Http\Request,
	Nette\Database\Connection;
use Nette\Utils\DateTime;

abstract class BaseRouter {
	protected static $table = "a_links";
	public $id;
	public $link;
	public $objectid;
	public $remap;
	public $original;
	public $status;
	public $model;
	public $date;
	public $action;
	public $requested_link;
	public $param;
	public $lang="sk";
	public $vyrobca_kategoria;
	public $domain="sk";
	public $domain_other="cz";
	public $link_params=array();
	public $query = [];

	/** @var  Connection */
	protected $db;
	/** @var  Request */
	protected $httpRequest;
	/** @var  IDiConfigurator */
	protected $context;

	protected $statusCodes = array(301 => "HTTP/1.0 301 Moved Pernamently", 302 => "HTTP/1.1 302 Found", 404=>"HTTP/1.0 404 Not Found", 200=>"HTTP/1.0 200 OK");
	public $sortNames = array(
		"sk" => array(
			"najlacnejsie" => "cena_asc",
			"najdrahsie" => "cena_desc",
			"najkvalitnejsie" => "cena_desc",
			"najpredavanejsie" => "predajnost",
			"najlepsie-hodnotene" => "hodnotenie",
			"abecedne" => "nazov",
		),
		"cz" => array(
			"nejlevnejsi" => "cena_asc",
			"nejkvalitnejsi" => "cena_desc",
			"nejprodavanejsi" => "predajnost",
			"nejoblibenejsi" => "hodnotenie",
			"abecedne" => "nazov",
		)
	);

	public function __construct($id = null){
			if(!((int)$id>0)){ return false; };
			$db = $this->db      = Service::get( "database" );
			if(\Cache::check('remap_links',$id)){ $R=\Cache::get('remap_links',$id); } else {
				$q = $db->query( "select * from " . "".static::$table."" . " where id=?", (int) $id );
				$f = $q->fetch();
				if($f) {
					$R = $f;
					\Cache::put('remap_links',$id,$R);
				};
			};
			if(isset($R)){
				$this->id=$R['id'];
				$this->link=$R['link'];
				$this->objectid=$R['objectid'];
				$this->remap=($R['remap']);
				$this->original=$R['original'];
				$this->status=$R['status'];
				$this->date=$R['date'];
				$this->model = $R["model"];
				$this->action = $R["action"];
				$this->lang = $R["lang"];
				/*if($this->objectid > 0){
					$this->object = new $this->model($this->objectid);
				}*/

			};

			$this->domain = "sk";
			$this->context = Service::get("container");
			if($this->context->isProtein()){
				if($this->context->isHost("protein.sk")){
					$this->domain = "sk";
					$this->domain_other = "cz";
				}
				if($this->context->isHost("protein.cz")){
					$this->domain = "cz";
					$this->domain_other = "sk";
				}
			}

			if(!$this->context->isCli()) {
				$host = $_SERVER["HTTP_HOST"];
				$this->httpRequest = Service::get( "request" );
			}

		
	}

	abstract public function findAlternate();
	abstract public function match();

	public function delete(){
		if($this->id){
			$this->db->query("delete from ".static::$table." where id=?",$this->id);
			\Cache::flush('remap_links');
		}
	}

	/**
	 * @param array $where
	 *
	 * @return array

	public static function find($where=[]){
		$q = \Registry::get("database")->query("SELECT * FROM ".static::$table." WHERE ",$where);
		$ret = [];
		foreach($q as $r){
			$ret[] = new static($r["id"]);
		}
		return $ret;
	}
	/**
	 * @param array $where
	 *
	 * @return array
	 */
	public static function find($where="true"){
		$q = \Registry::get("database")->query("SELECT * FROM ".static::$table." WHERE ".$where);
		$ret = [];
		foreach($q as $r){
			$ret[] = new static($r["id"]);
		}
		return $ret;
	}
	/**
	 * @param $objectid
	 * @param string $model = "Tovar"
	 * @param string $lang = "sk"
	 *
	 * @return static
	 */
	public static function findOriginal($objectid,$model="Tovar",$lang="sk"){
		$db= \Registry::get("database");
		$q = $db->query("select id from ".static::$table." where objectid=? and model=? and original='1' and lang=?",$objectid,$model,$lang);
		$r = $q->fetch();
		return new static($r["id"]);
	}

	/**
	 * @param $name
	 * @param int $i
	 *
	 * @return BaseRouter|static
	 */
	public static function findByLink($name,$i = 0){
		$db= \Registry::get("database");
		$q = $db->query("select id from ".static::$table." where link=?",strtolower($name));
		$r=$q->fetch();
		if(!$r){
			if(strpos($name,"/") !== false) {
				$explode = explode( "/", $name );
				$e       = end( $explode );
				$lnk     = str_replace( "/" . $e, "", $name );
				$i ++;
				if ( $i == 50 ) {
					return new static;
				}
				return self::findByLink( $lnk, $i );
			}

		}
		return new static($r["id"]);
	}

	/**
	 * @deprecated 
	 * @param $name
	 *
	 * @return BaseRouter
	 */
	public static function findByExclusiveLink($name){
		return static::findByLink($name);
	}

	public function save(){
		if(!$this->id){ $this->id=mysqli_new("".static::$table.""); $date = true;};
		$db = \Registry::get("database");
		$update = [];
		if($date) {
			//mysqli_query(\CommerceDB::$DB,"UPDATE ".static::$table." SET date = NOW() WHERE id=".$this->id);
			$update["date"] = new DateTime();
		}

		$update["link"] = $this->link;
		$update["objectid"] = $this->objectid;
		$update["model"] = $this->model;
		$update["action"] = $this->action;
		$update["remap"] = $this->remap;
		$update["original"] = $this->original;
		$update["status"] = $this->status;
		$update["lang"] = $this->lang;

		$db->query("UPDATE ".static::$table." SET ",$update,"WHERE id=?",$this->id);

		\Cache::del("remap_links",$this->id);
		\Cache::flush("remap_links","link_*");
	}
	public function createNew($new,$objectid,$model,$action,$lang="sk"){
		if($new != ""){
			$this->objectid = $objectid;
			$this->model = $model;
			$this->action = $action;
			$this->original = 0;
			$this->status = 301;
			$this->lang = $lang;
			if($this->id >0){
				$this->save();
			}

			$s = self::findByLink($new);
			//var_dump($s->id == null);
			$create = ($s->id !== null && $model != $s->model);
			if($s->id == null || $create){
				$n = new static;
				$n->link = $new;
				$n->objectid = $objectid;
				$n->model = $model;
				$n->action = $action;
				$n->original = 1;
				$n->status = 200;
				$this->lang = $lang;
				if($create){
					if(strtolower($model) == "vyrobca"){
						$n->link .= "-produkty";
					} elseif (strtolower($model) == "kategoria"){
						$n->link .= "-kategoria";
					} elseif (strtolower($model) == "tovar"){
						$n->link .= "-produkt";
					}
				}
				$n->save();
				return $n;
			}  else {
				if($s->objectid != $this->objectid){
					$s->link = $s->link."-produkt";
				}
				$s->status = 200;
				$s->original = 1;
				$s->save();
				return $s;
			}

			return $this;
		}
	}


}
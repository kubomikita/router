<?php
namespace Kubomikita\Commerce;
use Kubomikita\IRouter;
use Nette\DI\Container;

class DefaultRedirectsRouter  implements IRouter  {
	public static function create( Container $container ) {
		return new static();
	}
}
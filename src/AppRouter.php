<?php
namespace Kubomikita\Commerce;

use Kubomikita\Service;
use Nette\Http\Request,
	Nette\Database\Connection;

class AppRouter extends BaseRouter{



	public function findAlternate($checkHost = true){
		$SK = $CZ = true;
		if($checkHost){
			$CZ = $this->context->isHost("protein.cz");
			$SK = $this->context->isHost("protein.sk");
		}

		$sk_sortNames = $cz_sortNames = array();
		foreach($this->sortNames["sk"] as $k=>$v){
			$sk_sortNames[] = $k;
		}
		foreach($this->sortNames["cz"] as $k=>$v){
			$cz_sortNames[] = $k;
		}
		//dump($SK,$CZ,$this);
		//exit;
		if($SK and $this->lang == "cz"){
			$seolink_cz = static::findOriginal($this->objectid,$this->model,"sk");
			if($seolink_cz->id > 0 && $this->link != $seolink_cz->link){
				$seolink = $seolink_cz;
				$redirect = $seolink->link.str_replace($cz_sortNames,$sk_sortNames,$this->param);
				//echo $redirect;
				return $redirect;
			}
		}
		if($CZ and $this->lang == "sk"){
			//dump("slovensky link na protein.cz");
			$seolink_cz = static::findOriginal($this->objectid,$this->model,"cz");
			//dump($seolink_cz);
			if($seolink_cz->id > 0 && $this->link != $seolink_cz->link){
				$seolink = $seolink_cz;
				$redirect = $seolink->link.str_replace($sk_sortNames,$cz_sortNames,$this->param);
				//$this->redirect(301,$redirect);
				//exit;
				//echo $redirect;
				return $redirect;
			}
		}
		return false;
	}
	private function findSortNameAlternate(){
		$sk_sortNames = $cz_sortNames = array();
		foreach($this->sortNames["sk"] as $k=>$v){
			$sk_sortNames[] = $k;
		}
		foreach($this->sortNames["cz"] as $k=>$v){
			$cz_sortNames[] = $k;
		}

		$redirect = $this->link.str_replace(${$this->domain_other."_sortNames"},${$this->domain."_sortNames"},$this->param);
		return $redirect;
	}

	public function getAlternateLang(){

		$alt = $this->findAlternate(false);

		if($alt !== false){
			return $alt;
		}

		$ad = $this->domain;
		$da = $this->domain_other;

		$this->domain = $da;
		$this->domain_other = $ad;

		$return = $this->findSortNameAlternate();

		$this->domain = $ad;
		$this->domain_other = $da;

		return $return;//$this->link.$this->param;
	}
	public function match(){
		if($this->id == null){
			return false;
		}
		//dump($this);
		$seolink = $this->httpRequest->getQuery("seolink");

		$this->param = str_replace($this->link,"",strtolower($seolink));
		$this->requested_link = str_replace($this->param,"",$seolink);

		if($this->original){
			if($this->link != $this->requested_link){
				$this->redirect(301,$this->context->getParameter("shop","relurl").$this->link.strtolower($this->param));
				exit;
			}
			
			$page = $this->context->getParameter("page");
			$PAGE = $page[$this->action];

			/*$_REQUEST[strtolower($this->model)] = */$this->query[strtolower($this->model)] = $this->objectid;

			$params=explode("/",$this->param);
			$i=0; $sorting = false;
			foreach($params as $filter){
				if(strlen(trim($filter)) > 0){
					if(isset($this->sortNames[$this->domain][$filter]) && $this->sortNames[$this->domain][$filter] !== null){
						$_REQUEST["sort"] = $this->query["sort"] = $this->sortNames[$this->domain][$filter];
						$i++;
						$sorting = true;
						$this->link_params["sort"] = $filter;
					}
					$vyr = \TovarVyrobca::getBySeoName($filter);
					if($vyr instanceof \TovarVyrobca && $vyr->id > 0){
						$_REQUEST["vyrobcovia"] = $this->query["vyrobcovia"] = array($vyr->id);
						$i++;
						$this->link_params["manufacturer"] = $filter;
					}
					if(!$sorting){
						$kat = \Kategoria::getBySeoName($filter);
						if($kat instanceof \Kategoria && $kat->id >0){
							$_REQUEST["kat_seo"] = $this->query["kat_seo"] = $filter;
							$i++;
							$this->link_params["category"] = $filter;
							$this->vyrobca_kategoria = $kat;
						}
					}
					if($filter == "odporucame"){
						$_REQUEST["tag"] = $this->query["tag"] = "D";
						$i++;
						$this->link_params["recommend"] = $filter;
					}
					if(strpos($filter,"page-") !== false) {
						$page = (int) str_replace("page-","",$filter);
						if ( $page > 0 ) {
							$_REQUEST["page"] = $this->query["page"] = (int) $page;
							$i ++;
							$this->link_params["page"] = $filter;
						}
					}
				}
			}
			$alt =$this->findAlternate();
			if($alt !== false){
				//dump($alt);
				//exit;
				$this->redirect(301,$this->context->getParameter("shop","relurl").$alt);
				exit;
			}
			if(isset($sortAlternate) and $sortAlternate != $this->link.$this->param){
				$this->redirect(301,$this->context->getParameter("shop","relurl").$sortAlternate);
				exit;
			}


			if($i != (count($params)-1)/*$i == 0 and count($e) > 1*/){
				return false;
			}
			return $PAGE;

		}
		$this->redirect();
	}

	/**
	 * @param string $status
	 * @param string $link
	 */
	public function redirect($status="",$link=""){
		if($status != "" and $link !=""){
			header($this->statusCodes[$status]);
			header("location: ".$link);
			exit;
		} else {

			if($this->remap == ""){
				$original = self::findOriginal($this->objectid,$this->model,\LangStr::$locale);
				header($this->statusCodes[$this->status]);
				if(!empty($this->link_params)){
					header("location: ".$this->context->getParameter("shop","relurl").$original->link."/".implode("/",$this->link_params));
				} else {
					header("location: ".$this->context->getParameter("shop","relurl").$original->link/*.strtolower($this->param)*/);
				}
				exit;
			} else {
				header($this->statusCodes[$this->status]);
				if(strpos($this->remap,"http")!==false){
					header("location: ".$this->remap);
				} else {
					header("location: ".$this->context->getParameter("shop","relurl").$this->remap);
				}
				exit;
			}
		}
	}


	public function generateAlternate(){
		$param = preg_replace("~/page-[0-9]+~", "", $this->param);
		if($this->model == "Kategoria"){
			if(isset($this->link_params["manufacturer"]) and isset($this->link_params["sort"])){
				$param = str_replace("/".$this->link_params["manufacturer"],"",$param);
			}
			return $this->link.$param;
		}
		if($this->model == "Vyrobca"){
			$k = $this->vyrobca_kategoria;
			if($k != null){
				$link = eshop_kat_seo_link($k);
				if($this->link_params["sort"]!=""){
					return $link."/".$this->link_params["sort"];
				}
				return $link."/".$this->object->seo_name;
			}
		}
		return $this->link.$param;
	}
}
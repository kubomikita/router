<?php


namespace Kubomikita\Api;


use Kubomikita\Commerce\IDiConfigurator;
use Kubomikita\IRouter;
use Nette\Database\Connection;
use Nette\Http\IRequest;
use Nette\Http\Request;
use Nette\Http\Response;
use Nette\Reflection\ClassType;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nette\Utils\Json;
use Nette\Utils\Reflection;
use Nette\Utils\Strings;

abstract class BaseApi implements IApi{
	/** @var IDiConfigurator */
	protected $context;
	/** @var IRequest */
	protected $request;
	/** @var Response */
	protected $response;
	/** @var IRouter */
	protected $router;
	/** @var Connection */
	public $db;
	/** @var ArrayHash */
	protected $parameters = [];
	/** @var ArrayHash */
	protected $args = [];
	/** @var ArrayHash */
	protected $config = [];
	/** @var bool  */
	protected $logRequests = false;

	public $reflection;

	/**
	 * BaseApi constructor.
	 *
	 * @param IDiConfigurator $context
	 * @param IRouter $router
	 * @param IRequest $request
	 */
	public function __construct(IDiConfigurator $context, IRouter $router, IRequest $request) {
		$this->context = $context;
		$this->router = $router;
		$this->request = $request;
		$this->response = $context->getByType(Response::class);
		$this->db = $context->getByType(Connection::class);
		$this->parameters = ArrayHash::from($context->container->parameters);

		$this->reflection = new \ReflectionClass($this); //ClassType::from($this);

		$class = Strings::lower($this->reflection->getShortName());

		if(isset($this->parameters[$class])){
			$this->config = $this->parameters[$class];
		}
		$this->reflection = ClassType::from($this);
	}

	/**
	 * @param array $args
	 *
	 * @return $this
	 */
	public function setArgs(array $args = []){
		$escaped = [];
		foreach($args as $k=>$v){
			$escaped[$k] = (!is_array($v) ? Strings::substring($this->db->getPdo()->quote($v),1,-1) : $v);
		}
		$this->args = ArrayHash::from($escaped);
		return $this;
	}

	/**
	 * @return ArrayHash
	 */
	public function getArgs() {
		return $this->args;
	}

	/**
	 * @param IRequest $request
	 * @param \Throwable|null $e
	 */
	public function logRequest( IRequest $request, ?\Throwable $e = null) : void
	{
		if($this->logRequests) {
			$content = $request->getRawBody();
			$content_lenght = strlen($content);
			$content_type = $content_lenght > 0 ? $request->getHeader("content-type") : null;

			$url = $request->getUrl();

			$this->db->query("INSERT INTO api_requests",[
				"datetime" => new DateTime(),
				"method" => $request->getMethod(),
				"ip" => $request->getRemoteAddress(),
				"url" => str_replace($request->getUrl()->getHostUrl(),"",$request->getUrl()->getAbsoluteUrl()),
				"headers" => json_encode($request->getHeaders()),
				"useragent" => $request->getHeader("user-agent"),
				"content_type" => $content_type,
				"content_lenght" => $content_lenght,
				"exception_message" => ($e instanceof \Throwable) ? $e->getMessage() : null,
				"exception" => ($e instanceof \Throwable) ? json_encode($e) : null,
			]);
		}
	}

	/**
	 * @param string $rawBody
	 *
	 * @return string
	 */
	public function getMimeTypeOfRawBody(string $rawBody) : string
	{
		$temp_file = tempnam(sys_get_temp_dir(), 'request');
		file_put_contents($temp_file , $content);
		//$finfo = finfo_open(FILEINFO_MIME_TYPE);
		mime_content_type($temp_file);
		return mime_content_type($temp_file);//finfo_file($finfo, $temp_file);
	}

	/**
	 * @return void
	 */
	abstract public function beforeStartup();
	
	public function authenticate():void
	{

	}

	/**
	 * @param string $body
	 */
	public function sendXmlResponse(string $body) : void
	{
		header( 'Content-type: text/xml' );
		echo $body;
		exit;
	}

	/**
	 * @param array $body
	 *
	 * @throws \Nette\Utils\JsonException
	 */
	public function sendJsonResponse(array $body) : void
	{
		header( 'Content-type: application/json' );
		echo Json::encode($body);
		exit;
	}

}